const myTabsSwitcher = document.getElementsByClassName("tabs-title-button");
for (let i = 0; i < myTabsSwitcher.length; i++) {
    myTabsSwitcher[i].addEventListener('click', switchMyTabs);
}
function switchMyTabs() {
    console.log(this)
    const tabs = this.parentElement;
    const tabsChildrenElement = tabs.children;
    for (let i = 0; i < tabsChildrenElement.length; i++ ){
     tabsChildrenElement[i].classList.remove('active');
    }
    this.classList.add('active');
    const showTabs = document.getElementById(this.dataset.target);
    const show = showTabs.parentElement;

    const showChildren = show.children;
    for (let i = 0; i < showChildren.length; i++) {
        showChildren[i].classList.remove('tabs-content-box-active');
    }
    showTabs.classList.add('tabs-content-box-active');
}
